from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from django.http import HttpResponse
from django.urls import reverse_lazy

from todos.models import TodoItem, TodoList

# Create your views here.

class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model =TodoList
    template_name = "todos/detail.html"

    def get_success_url(self):
        return reverse_lazy("todolist_detail", args=[self.object.id])


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
    
    def get_success_url(self):
        return reverse_lazy("todolist_detail", args=[self.object.id])
    

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todolist_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todolist_list")


#TodoItems ------

class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/items/create.html"
    fields = ["task", "due_date", "is_completed", "list"]
    
    def get_success_url(self):
        return reverse_lazy("todolist_detail", args=[self.object.list.id])
    

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/items/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todolist_detail")

    def get_success_url(self):
        return reverse_lazy("todolist_detail", args=[self.object.list.id])

class TodoItemDeleteView(DeleteView):
    model = TodoItem
    template_name = "todos/items/delete.html"
    success_url = reverse_lazy("todolist_detail")

    def get_success_url(self):
        return reverse_lazy("todolist_detail", args=[self.object.list.id])
    
    






    # def some_view(request):
    #     response = HttpResponse('some data')
    #     response.status_code = 200  
    #     return response
