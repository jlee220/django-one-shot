from django.urls import path

from todos.views import (
    TodoListListView,    
    TodoListDetailView,
    TodoListCreateView,
    TodoListDeleteView,
    TodoListUpdateView,
    TodoItemCreateView,
    TodoItemDeleteView,
    TodoItemUpdateView,
)


urlpatterns = [
    path("", TodoListListView.as_view(), name="todolist_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todolist_detail"), 
    path("create/", TodoListCreateView.as_view(), name="todolist_create_list"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todolist_edit"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todolist_delete"),
    path("items/create/", TodoItemCreateView.as_view(), name="todoitem_create_list"),
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name="todoitem_edit"),
    path("items/<int:pk>/delete/", TodoItemDeleteView.as_view(), name="todoitem_delete"),
    
]
